/*
 * prints number of processors
 */

#include <unistd.h>

#include "common.h"

int main(int argc, char **argv) {
  if (argc!=1) fail("usage: nprocessors\n");
  long sc= sysconf(_SC_NPROCESSORS_ONLN);
  if (sc==-1) diee("nprocessors: sysconf failed");
  printf("%ld\n",sc);
  flushoutput();
  return 0;
}
