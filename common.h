/*
 * Generally useful stuff.
 */

#ifndef COMMON_H
#define COMMON_H

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 1000
#endif

#include <sys/types.h>
#include <sys/stat.h>

#include <math.h>
#include <float.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_interp.h>

#define D3 3

double hypotD(const double p[D3], const double q[D3]);
double hypotD2(const double p[D3], const double q[D3]);
double hypotD2plus(const double p[D3], const double q[D3], double add);

double magnD(const double pq[D3]);
double magnD2(const double pq[D3]);
void xprod(double r[D3], const double a[D3], const double b[D3]);
double dotprod(const double a[D3], const double b[D3]);
void xprod_norm(double r[D3], const double a[D3], const double b[D3],
		double one, double absepsilon);
void normalise(double v[D3], double one, double absepsilon);

void flushoutput(void);
void diee(const char *what);
void fail(const char *emsg);

void libdie(const char *lib, int l, const char *str);
#define XA(w) ((w) ? (void)0 : libdie("X", __LINE__, #w))

void gsldie(int l, const char *what, int status);
#define GA(w) ({int _ga_r= (w); if (w) gsldie(__LINE__,#w,_ga_r);})

#define FOR_COORD(k) \
  for ((k)=0; (k)<D3; (k)++)

#define K FOR_COORD(k)

#define GSL_VECTOR(x) gsl_vector x##_gsl= { D3,1,&x[0] };
#define GSL_MATRIX(x) gsl_matrix x##_gsl= { D3,D3,D3,&x[0][0] };

#define MIN(a,b) ((a) <= (b) ? (a) : (b))
#define MAX(a,b) ((a) >= (b) ? (a) : (b))

#ifdef FP_FAST_FMA
# define fma_fast fma
#else
# define fma_fast(f1,f2,t) ((f1)*(f2)+(t))
#endif
#define ffsqa(factor,term) fma_fast((factor),(factor),(term))

#endif /*COMMON_H*/
