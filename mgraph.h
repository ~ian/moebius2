/*
 * Graph topology
 */
/*
 * Vertices in strip are numbered as follows:
 *
 *                          :axis of symmetry
 *			  | :
 *			  | :
 *     ___ X-2 ___ X-1 ___| 0  ___  1  ___  2  ___  3  ___  4  __
 *         Y-1     Y-1    |0:      0       0       0       0
 *        /  \    /  \    / :\    /  \    /  \    /  \    /  \
 *       /    \  /    \  /| : \  /    \  /    \  /    \  /    \
 *     X-3 ___ X-2 ___ X-1|___  0  ___  1  ___  2  ___  3  ___  4
 *     Y-2     Y-2     Y-2| :  1       1       1       1       1
 *       \    /  \    /  \| : /  \    /  \    /  \    /  \    /
 *        \  /    \  /    \ :/    \  /    \  /    \  /    \  /
 *     ___ X-2 ___ X-1 ___| 0  ___  1  ___  2  ___  3  __   4 ___
 *         Y-3     Y-3    |2:      2       2       2 	   2
 *        /  \    /  \    / :\    /  \    /  \    /  \    /  \
 *       /    \  /    \  /| : \  /    \  /    \  /    \  /    \
 *     X-3 ___ X-2 ___ X-1|___  0  ___  1  ___  2  ___  3  ___  4
 *     Y-4     Y-4     Y-4| :  3       3       3       3       3
 *			  | :
 *       .   .   .   .   .| :.   .   .   .   .   .   .   .   .   .
 *			  | :
 *     ___ X-2 ___ X-1 ___| 0  ___  1  ___  2  ___  3  ___  4 ___
 *         2       2      |Y-3     Y-3     Y-3     Y-3     Y-3
 *        /  \    /  \    / :\    /  \    /  \    /  \    /  \
 *       /    \  /    \  /| : \  /    \  /    \  /    \  /    \
 *          __ X-2 ___ X-1|___  0  ___  1  ___  2  ___  3  ___  3  ___  4
 *             1       1  | :  Y-2     Y-2     Y-2     Y-2     Y-2     Y-2
 *    /  \    /  \    /  \| : /  \    /  \    /	 \    /  \    /
 *   /    \  /    \  /    \ :/    \  /    \  /	  \  /    \  /
 *  -3 ___ X-2 ___ X-1 ___| 0  ___  1  ___  2  ___  3  ___  4 ___
 *  0      0       0      |Y-1     Y-1     Y-1     Y-1     Y-1
 *			  | :
 *			  | :
 *			  |
 *                        ^ join, where there is
 *                           a discontinuity in numbering
 *
 * Node x,y for
 *   0 <= x < X = 2^XBITS     x = distance along
 *   0 <= y < Y = 2^YBITS-1   y = distance across
 *
 * Vertices are in reading order from diagram above ie x varies fastest.
 *
 * Note that though presentation above is equilateral triangles, this
 * is not the case.  It's actually a square lattice with half of the
 * diagonals added.  We can't straighten it out because at the join
 * the diagonals point the other way!
 *
 * We label edges as follows:
 *
 *		    \2   /1
 *		     \  /
 *		  ___ 0   __
 *		  3    1   0
 *		     /  \
 *		   4/   5\
 *
 * vertex number:   0000 | y      | x
 *                        (YBITS)   XBITS
 */

#ifndef MGRAPH_H
#define MGRAPH_H

#include "common.h"

#ifndef DEFSZ /* DEFSZ may be (Y/2-1)*10 + XBITS ie Y is 2*<tens>+1 */
#define XBITS 3
#define YBITS 3
#define Y ((1<<YBITS) - 1)
#define YMASK (Y << YSHIFT)
#else
#define XBITS  (DEFSZ % 10)
#define Y      ((DEFSZ / 10)*2+1)
#endif

#define X (1<<XBITS)

#define N (X*Y)
#define XMASK (X-1)
#define YSHIFT XBITS
#define Y1 (1 << YSHIFT)

#define V6 6
#define V3 3


/* Loop constructors are macros of the form
 *    LOOP(v,zero,n, precomp)
 * which work much like this one:
 */
#define INNER(v,zero,n, precomp)  \
  for ((v)=(zero); precomp, (v)<(n); (v)++)

#define FOR_VERTEX(v,loop) \
  loop ((v), 0, N, NOTHING)

#define FOR_VPEDGE(e) \
  for ((e)=0; (e)<V6; (e)++)

extern short edge_end2_memo[][V6];
#define EDGE_END2(v1,e) edge_end2_memo[v1][e]

/* given    v1,e     s.t.  v2==EDGE_END2(v1,e) >= 0,
 * returns  eprime   s.t.  v1==EDGE_END2(v2,eprime) */
int edge_reverse(int v1, int e);

#define EDGE_OPPOSITE(e) (((e)+V3) % V6)

#define RIM_VERTEX_P(v) (((v) & ~XMASK) == 0 || ((v) & ~XMASK) == (Y-1)*Y1)

#define FOR_VEDGE_X(v1,e,v2,init,otherwise)	\
  FOR_VPEDGE((e))				\
    if (((v2)= EDGE_END2((v1),(e)),		\
	 (init),				\
	 (v2)) < 0) { otherwise; } else

#define NOTHING ((void)0)

#define FOR_VEDGE(v1,e,v2)			\
  FOR_VEDGE_X(v1,e,v2,NOTHING,NOTHING)

#define FOR_EDGE(v1,e,v2, loop)			\
  FOR_VERTEX((v1), loop)			\
    FOR_VEDGE((v1),(e),(v2))

#define FOR_NEAR_RIM_VERTEX(vy,vx,v, disttorim, loop)		\
  for ((vy)=(disttorim); (vy)<Y; (vy)+=Y-1-2*(disttorim))	\
    loop ((vx), 0, X, (v)= (vy)<<YSHIFT | (vx))

#define FOR_RIM_VERTEX(vy,vx,v, loop)		\
  FOR_NEAR_RIM_VERTEX((vy),(vx),(v), 0, loop)

int vertices_span_join_p(int v0, int v1);

typedef double Vertices[N][D3];
struct Vertices { Vertices a; };

void mgraph_prepare(void);

#endif /*MGRAPH_H*/
