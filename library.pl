sub run ($) {
    print "    $_[0]\n";
    $!=0; system($_[0]); die "$! $?" if $! or $?;
}

sub initdata_process_prime() {
    my ($i,$fmt);
    open P, "$prime" or die $!;
    $!=0; $_= <P>; m/^(\d+) (\d+) .*/ or die "$_ ? $!";
    ($dim,$nvertices)= ($1,$2);
    $!=0; $_= <P>; m/^(\%.*)\n/ or die "$_ ? $!";
    $fmt= $1;
    for ($i=0; $i<$nvertices; $i++) {
	$!=0; defined($_=<P>) or die "$prime $!";
	m/^\s*(\S+)\s*(\S+)\s*(.*)/ or die "$_ ?";
	initdata_prime_point_callback($1,$2,$3,$fmt);
    }
}

sub make_initdata () {
    open I, "$initdata" or die "$initdata $!";
    open B, ">$output.new" or die "$output.new $!";

    for ($i=0; $i<$nvertices; $i++) {
	for ($k=0; $k<3; $k++) {
	    for (;;) {
		$!=0; defined($_= <I>) or die "$initdata $!";
		last if m/\S/;
	    }
	    $vertex[$k]= $_;
	}
	initdata_return_vertex_transform();
	print B pack "d3", @vertex or die $!;
    }

    close B or die $!;
    rename "$output.new",$output or die $!;

    print "    wrote $output\n";
}

1;
