/*
 * Generates an STL file
 *  usage: ./output+<size> <thickness> <unit-circle-diameter>
 *    thickness is in original moebius unit circle units
 *    unit circle diameter is in whatever dimensions the STL file uses
 *
 * We have to:
 *   - add some thickness
 *   - define a shape for the rim so that it is solid
 *   - scale the coordinates
 *   - translate the coordinates so they're all positive
 */
/*
 *  ./output-64 <dense-64.cfm 0.1 50 >t.stl
 *  ./output-125 <best-125.cfm >t.stl 0.1 50
 *  meshlab t.stl
 */
/*
 * Re STL, see:
 *  http://www.ennex.com/~fabbers/StL.asp
 *  http://en.wikipedia.org/wiki/STL_%28file_format%29
 *    saved here as stl-format-wikipedia.txt
 */

/*
 * We add thickness by adding vertices as follows:
 *
 *	               :axis of symmetry
 *	       sides | :
 *	         swap| :
 *	             | :
 *       *F          | : *F              *F              *F              *F
 *                   | :
 *      *E      *G   | :*E      *G      *E      *G      *E      *G      *E
 *                   | :
 *    *D ____________|*D ____________ *D ____________ *D ____________ *D __
 *    /\          3  |/\   0          /\              /\              /\
 *   /  \            / :\            /  \            /  \            /  \
 *       \    *A   4/ \: \5   *B    /    \    *B    /    \    *B    /    \
 *        \        /   \  \    1   /      \        /      \        /
 *         \      /    :\  \      /        \      /        \      /
 *    *B    \    /    *B2|  \2  1/   0*A    \    /    *A    \    /    *A
 *           \  /      : |   \  / . '        \  /            \  /
 *   _______  *C _____ :_|___ *C' __________  *C ___________  *C _________
 *            /\       : | 3  /\.  0          /\              /\
 *           /  \      :3|   /  \ ` .        /  \            /  \
 *    *A    /    \    *A |  /4  5\   5*B    /    \    *B    /    \    *B
 *         /      \   1: / /      \        /      \        /      \
 *        /        \   :/ /    4   \      /        \      /        \
 *       /    *B2  2\  / /1   *A    \    /    *A    \    /    *A    \    /
 *   \  /            \/:/     0      \  /            \  /            \  /
 *    *C ____________|*C ____________ *C ___________  *C ___________  *C __
 *    /\          3  |/\   0          /\              /\              /\
 *   /  \            / :\            /  \            /  \            /  \
 *       \    *A3  4/ \: \5  5*B    /    \    *B    /    \    *B    /    \
 *        \        /   \  \        /      \        /      \        /
 *         \      /   4:\  \      /        \      /        \      /
 *    *B    \    /    *B |  \    /    *A    \    /    *A    \    /    *A
 *           \  /      : |   \  / . '        \  /            \  /
 *   _______  *C _____ :_|___ *C' __________  *C ___________  *C _________
 *            /\       : | 3  /\   0          /\              /\
 *           /  \      : |   /  \            /  \            /  \
 *                     :
 *         .      .    :   .      .        .      .        .      .
 *                     :

 *           \  /      : |   \  /            \  /            \  /
 *   _______  *C _____ :_|___ *C ___________  *C ___________  *C _________
 *            /\       : | 3  /\   0          /\              /\
 *           /  \      : |   /  \            /  \            /  \
 *    *A    /    \    *A |  /4  5\    *B    /    \    *B    /    \    *B
 *         /      \   1: / /      \        /      \        /      \
 *        /        \   :/ /        \      /        \      /        \
 *       /    *B2  2\  / /1  0*A    \    /    *A    \    /    *A    \    /
 *   \  /            \/:/            \  /            \  /            \  /
 *    *D ____________|*D ____________ *D ___________  *D ___________  *D __
 *     	           3 | :   0
 *      *E    *G     | :*E    *G        *E    *G        *E    *G        *E
 *                   | :
 *       *F          | : *F              *F              *F              *F
 *	             | :
 *
 * Each A,B,C,D represents two vertices - one on each side of the
 * surface.  Each E,F,G represents a several vertices in an arc around
 * the rim.  Digits are `e' values for edges or relative AB
 * outvertices.
 *
 * The constructions we use are:
 *
 *  A, B:  Extend the normal vector of the containing intriangle
 *         from its centroid for thickness.
 *
 *  C:     Take mean position (centroid) of all surrounding
 *         computed A and B, and extend from base point in
 *         direction of that centroid for thickness.
 *
 *  D, E, F:
 *         Compute notional rim vector R as mean of the two
 *         adjoining rim edges, and consider plane P as
 *         that passing through the base point normal to R.
 *         Project the centroid of the two adjoining non-rim
 *         vertices onto P.  Now D(EF)+ED is the semicircle
 *         in P centred on the base point with radius thickness
 *         and which is opposite that centroid.
 *
 *  G:     Each F is the mean of those two adjacent Es with the
 *         same angle in their respective Ps.
 *
 * The outfacets are:
 *
 *  For each input vertex on each side, the six (or perhaps only three)
 *  triangles formed by its C or D and the surrounding Cs and/or Ds.
 *
 *  For each G, the six triangles formed by that G and the adjacent
 *  four Fs (or two Fs and two Ds) and two Es.
 */

#include "mgraph.h"
#include "common.h"

/*---------- declarations and useful subroutines ----------*/

#define FOR_SIDE for (side=0; side<2; side++)

typedef struct {
  double p[D3];
} OutVertex;

#define NG   10
#define NDEF (NG*2+1)

#define OUTPUT_ARRAY_LIST(DO_OUTPUT_ARRAY) 	\
  DO_OUTPUT_ARRAY(ovAB)				\
  DO_OUTPUT_ARRAY(ovC)				\
  DO_OUTPUT_ARRAY(ovDEF)			\
  DO_OUTPUT_ARRAY(ovG)

static OutVertex ovAB[N][2][2]; /* indices: vertex to W, A=0 B=1, side */
static OutVertex ovC[N][2];
static OutVertex ovDEF[X][2][NDEF]; /* x, !!y, angle */
static OutVertex ovG[X][2][NG]; /* x, !!y, angle; for G to the East of x */

static Vertices in;

static double thick; /* in input units */
static double scale;

static void outfacet(int rev, const OutVertex *a,
		     const OutVertex *b, const OutVertex *c);

typedef int int_map(int);
static int defs_aroundmap_swap(int around) { return NDEF-1-around; }
static int int_identity_function(int i) { return i; }

static void normalise_thick(double a[D3]) {
  /* multiplies a by a scalar so that its magnitude is thick */
  int k;
  double multby= thick / magnD(a);
  K a[k] *= multby;
}

static void triangle_normal(double normal[D3], const double a[D3],
			    const double b[D3], const double c[D3]) {
  double ab[D3], ac[D3];
  int k;
  
  K ab[k]= b[k] - a[k];
  K ac[k]= c[k] - a[k];
  xprod(normal,ab,ac);
}

static OutVertex *invertex2outvertexab(int v0, int e, int side) {
  int vref, vchk, ab;
  switch (e) {
  case 0:  vref=v0;               vchk=EDGE_END2(v0,1);  ab=0;   break;
  case 1:  vref=                  vchk=EDGE_END2(v0,2);  ab=1;   break;
  case 2:  vref=EDGE_END2(v0,3);  vchk=EDGE_END2(v0,2);  ab=0;   break;
  case 3:  vref=EDGE_END2(v0,3);  vchk=EDGE_END2(v0,4);  ab=1;   break;
  case 4:  vref=                  vchk=EDGE_END2(v0,4);  ab=0;   break;
  case 5:  vref=v0;               vchk=EDGE_END2(v0,5);  ab=1;   break;
  default: abort();
  }
  if (vchk<0) return 0;
  int sw= vertices_span_join_p(v0,vref);
  return &ovAB[vref][ab^sw][side^sw];
}

/*---------- output vertices ----------*/

#define Ok(ov, value) ((ov).p[k]= outvertex_coord_check(value))

static double outvertex_coord_check(double value) {
  assert(-10 < value && value < 10);
  return value;
}

static void compute_outvertices(void) {
  int v0,k,side,ab,x,y;

  FOR_VERTEX(v0, INNER) {
    for (ab=0; ab<2; ab++) {
      int v1= EDGE_END2(v0, ab?5:0);
      int v2= EDGE_END2(v0, ab?0:1);
      if (v1<0 || v2<0)
	continue;
      double normal[D3], centroid[D3];
      triangle_normal(normal, in[v0],in[v1],in[v2]);
      normalise_thick(normal);
      K centroid[k]= (in[v0][k] + in[v1][k] + in[v2][k]) / 3.0;
      K Ok(ovAB[v0][ab][0], centroid[k] + normal[k]);
      K Ok(ovAB[v0][ab][1], centroid[k] - normal[k]);
    }
  }
  FOR_VERTEX(v0, INNER) {
    int vw= EDGE_END2(v0,3);
    int vnw= EDGE_END2(v0,2);
    int vsw= EDGE_END2(v0,4);
    if (vnw<0 || vsw<0 || vw<0)
      continue;
    FOR_SIDE {
      double adjust[D3];
      int e;
      K adjust[k]= 0;
      FOR_VPEDGE(e) {
	OutVertex *ovab= invertex2outvertexab(v0,e,side);
	K {
	  assert(!isnan(ovab->p[k]));
	  adjust[k] += ovab->p[k];
	}
      }
      K adjust[k] /= 6;
      K adjust[k] -= in[v0][k];
      normalise_thick(adjust);
      K Ok(ovC[v0][side], in[v0][k] + adjust[k]);
    }
  }
  FOR_RIM_VERTEX(y,x,v0, INNER) {
    double rim[D3], inner[D3], radius_cos[D3], radius_sin[D3];
    int vback, vfwd, around;

    /* compute mean rim vector, which is just the vector between
     * the two adjacent rim vertex (ignoring the base vertex) */
    vback= EDGE_END2(v0,3);
    vfwd=  EDGE_END2(v0,0);
    assert(vback>=0 && vfwd>=0);
    K rim[k]= in[vfwd][k] - in[vback][k];

    /* compute the inner centroid */
    vback= EDGE_END2(v0,4);
    if (vback>=0) { /* North rim */
      vfwd= EDGE_END2(v0,5);
    } else { /* South rim */
      vback= EDGE_END2(v0,2);
      vfwd=  EDGE_END2(v0,1);
    }
    assert(vback>=0 && vfwd>=0);
    K inner[k]= (in[vback][k] + in[vfwd][k]) / 2;
    K inner[k] -= in[v0][k];

    /* we compute the radius cos and sin vectors by cross producting
     * the vector to the inner with the rim, and then again, and
     * normalising. */
    xprod(radius_cos,rim,inner);
    xprod(radius_sin,rim,radius_cos);
    normalise_thick(radius_cos);
    normalise_thick(radius_sin);

    int_map *around_map= y ? int_identity_function : defs_aroundmap_swap;

    for (around=0; around<NDEF; around++) {
      double angle= around_map(around) * M_PI / (NDEF-1);
      K Ok(ovDEF[x][!!y][around],
	   in[v0][k] +
	   cos(angle) * radius_cos[k] +
	   sin(angle) * radius_sin[k]);
    }
  }
  FOR_RIM_VERTEX(y,x,v0, INNER) {
    int vfwd= EDGE_END2(v0,0);
    assert(vfwd >= 0);
    int aroung;
    int_map *around_map= vertices_span_join_p(v0,vfwd)
      ? defs_aroundmap_swap : int_identity_function;
    for (aroung=0; aroung<NG; aroung++) {
      K Ok(ovG[x][!!y][aroung],
	   0.5 * (ovDEF[ x          ][!! y             ][aroung*2+1].p[k] +
		  ovDEF[vfwd & XMASK][!!(vfwd & ~XMASK)][around_map(aroung*2+1)].p[k]));
    }
  }
}

/*---------- output facets ----------*/

static void outfacets_around(int reverse, OutVertex *middle,
			     int nsurr, OutVertex *surround[nsurr]) {
  /* Some entries in surround may be 0, in which case all affected
   *  facets will be skipped */
  int i;
  for (i=0; i<nsurr; i++) {
    OutVertex *s0= surround[i];
    OutVertex *s1= surround[(i+1) % nsurr];
    if (!s0 || !s1) continue;
    outfacet(reverse, middle,s0,s1);
  }
}

static OutVertex *invertex2outvertexcd(v0,side) {
  if (!RIM_VERTEX_P(v0)) return &ovC[v0][side];

  int around= side ? NDEF-1 : 0;
  int rimy= !!(v0 & ~XMASK);
  return &ovDEF[v0 & XMASK][rimy][around];
}

static void outfacets(void) {
  int v0,e,side,aroung;
  
  FOR_VERTEX(v0, INNER) {
    OutVertex *defs=0, *defs1=0;
    int rimy=-1;
    int_map *defs1aroundmap= 0;
    if (RIM_VERTEX_P(v0)) {
      OutVertex *gs;
      rimy= !!(v0 & ~XMASK);
      int v1= EDGE_END2(v0,0);  assert(v1>=0);
      gs=    ovG  [v0 & XMASK][rimy];
      defs=  ovDEF[v0 & XMASK][rimy];
      defs1= ovDEF[v1 & XMASK][!!(v1 & ~XMASK)];
      defs1aroundmap= vertices_span_join_p(v0,v1)
	? defs_aroundmap_swap : int_identity_function;

      for (aroung=0; aroung<NG; aroung++) {
	int around= aroung*2;
	OutVertex *surround[6];
	for (e=0; e<3; e++) {
	  surround[e  ]= &defs1[defs1aroundmap(around  +e)];
	  surround[e+3]= &defs [               around+2-e ];
	}
	outfacets_around(rimy, &gs[aroung], 6,surround);
      }
    }

    FOR_SIDE {
      int ab;
      for (ab=0; ab<2; ab++) {
	int v1= EDGE_END2(v0, ab ? 5 : 0);
	int v2= EDGE_END2(v0, ab ? 0 : 1);
	if (v1<0 || v2<0) continue;
	outfacet(side,
		 invertex2outvertexcd(v0,side),
		 invertex2outvertexcd(v1,side^vertices_span_join_p(v0,v1)),
		 invertex2outvertexcd(v2,side^vertices_span_join_p(v0,v2)));
      }
    }
  }
}     

/*---------- operations on very output vertex ----------*/

#define DO_OUTPUT_ARRAY_OUTVERTEX_ARRAY(fn,ovX) \
  ((fn)(sizeof((ovX))/sizeof(OutVertex), (OutVertex*)(ovX)))

static void blank_outvertex_array(int n, OutVertex ovX[n]) {
  int i, k;
  for (i=0; i<n; i++)
    K ovX[i].p[k]= NAN;
}

static void blank_outvertices(void) {
#define BLANK_OUTPUT_ARRAY(ovX) \
      DO_OUTPUT_ARRAY_OUTVERTEX_ARRAY(blank_outvertex_array, ovX);
  OUTPUT_ARRAY_LIST(BLANK_OUTPUT_ARRAY)
}

static void transform_outvertex_array(int n, OutVertex ovX[n]) {
  int i, k;
  for (i=0; i<n; i++) {
    ovX[i].p[0] *= -1;
    ovX[i].p[1] *= -1;
    K ovX[i].p[k] *= scale;
  }
  /*
   *   double min[D3]= thick;
   *		if (ovX[i].p[k] < min)
   *	    min= ovX[i].p[k];
   *      for (i=0; i<n; i++) {
   *  K ovX[k].p[k] -= min;
   */
}

static void transform_outvertices(void) {
#define TRANSFORM_OUTPUT_ARRAY(ovX) \
      DO_OUTPUT_ARRAY_OUTVERTEX_ARRAY(transform_outvertex_array, ovX);
  OUTPUT_ARRAY_LIST(TRANSFORM_OUTPUT_ARRAY)
}

/*---------- output file ----------*/

static void wr(const void *p, size_t sz) {
  if (fwrite(p,sz,1,stdout) != 1)
    diee("fwrite");
}

#define WR(x) wr((const void*)&(x), sizeof((x)))

static void wf(double d) {
  typedef float ieee754single;

  assert(sizeof(ieee754single)==4);
  assert(!isnan(d));

assert(d >= -1e3 && d <= 1e3);

#if BYTE_ORDER==BIG_ENDIAN
  union { uint8_t b[4]; ieee754single f; } value;  value.f= d;
  int i;  for (i=3; i>=0; i--) WR(value.b[i]);
#elif BYTE_ORDER==LITTLE_ENDIAN
  ieee754single f= d;
  WR(f);
#else
# error not little or big endian!
#endif
}

static uint32_t noutfacets;
static uint32_t noutfacets_counted;
static long badfacets;

static void outfacet(int rev, const OutVertex *a,
		     const OutVertex *b, const OutVertex *c) {
  if (rev) { outfacet(0, c,b,a); return; }

  double normal[D3];
  int k;
  
  K {
    assert(!isnan(a->p[k]));
    assert(!isnan(b->p[k]));
    assert(!isnan(c->p[k]));
  }

  triangle_normal(normal, a->p, b->p, c->p);
  double multby= 1/magnD(normal);
  
  if (multby > 1e6) {
    badfacets++;
    return;
  }

  noutfacets++;
  if (!~noutfacets_counted) return;
  
  K normal[k] *= multby;

  K wf(normal[k]);
  K wf(a->p[k]);
  K wf(b->p[k]);
  K wf(c->p[k]);
  uint16_t attrbc=0;
  WR(attrbc);
}

static void write_file(void) {
  static const char header[80]= "#!/usr/bin/meshlab\n" "binary STL file\n";

  if (isatty(1)) fail("will not write binary stl to tty!\n");

  WR(header);

  noutfacets_counted=~(uint32_t)0;
  noutfacets=0;
  outfacets();
  WR(noutfacets);
  
  noutfacets_counted= noutfacets;
  noutfacets=0;
  outfacets();
  assert(noutfacets == noutfacets_counted);

  if (fflush(stdout)) diee("fflush stdout");

  if (badfacets) {
    fprintf(stderr,"%ld degenerate facets!\n",badfacets);
    exit(4);
  }
}  
  

/*---------- main program etc. ----------*/

int main(int argc, const char *const *argv) {
  int r;

  if (argc!=3 || argv[1][0]=='-') { fputs("bad usage\n",stderr); exit(8); }
  thick= atof(argv[1]);
  scale= atof(argv[2]) * 0.5; /* circle is unit radius but arg is diameter */

  errno= 0; r= fread(&in,sizeof(in),1,stdin);
  if (r!=1) diee("fread");

  mgraph_prepare();
  blank_outvertices();
  compute_outvertices();
  transform_outvertices();
  write_file();
  if (fclose(stdout)) diee("fclose stdout");
  return 0;
}
