/*
 * graph layout energy
 */

#include "mgraph.h"
#include "minimise.h"
#include "parallel.h"

static int sqdistances[N][N];

static double alpha, beta, beta_prime;

static void breadth_first_search(int start, int sqdistances_r[N]) {
  int d[N], buffer[N], *buf_pop=buffer, *buf_push=buffer;
  int v,e, current, future, dfuture;

  buf_push= buf_pop= buffer;
  FOR_VERTEX(v,INNER) d[v]= -1;

  d[start]= 0;
  *buf_push++= start;

  while (buf_pop < buf_push) {
    current= *buf_pop++;
    dfuture= d[current] + 1;
    FOR_VEDGE(current,e,future) {
      if (d[future] >= 0) continue; /* already found this one */
      d[future]= dfuture;
      *buf_push++= future;
    }
  }
  assert(buf_pop==buf_push);
  assert(buf_push <= buffer+sizeof(buffer)/sizeof(buffer[0]));

  FOR_VERTEX(v,INNER) {
    assert(d[v] >= 0);
    sqdistances_r[v]= d[v] * d[v];
  }

//  FOR_VERTEX(v) {
//    
}

void graph_layout_prepare(void) {
  int v1;
  
  FOR_VERTEX(v1,INNER)
    breadth_first_search(v1, sqdistances[v1]);

  alpha= 2;
  beta= log(10)/log(alpha);
  beta_prime= (1-beta)/2;
  printf("alpha=%g beta=%g beta'=%g\n", alpha,beta,beta_prime);
}


double graph_layout_cost(const Vertices v, int section) {
  /* For each (vi,vj) computes shortest path s_ij = |vi..vj|
   * along edges, and actual distance d_ij = |vi-vj|.
   *
   * We will also use the `vertex areas': for each vertex vi the
   * vertex area a_vi is the mean area of the incident triangles.
   * This is computed elsewhere.
   *
   * Energy contribution is proportional to
   *
   *               -4          2
   *    a  a   .  d   . [ (s/d)  - 1 ]
   *     vi vj
   *
   * (In practice we compute d^2+epsilon and use it for the
   *  divisions, to avoid division by zero.)
   */
  //static const double d2_epsilon= 1e-6;

  double total_cost=0;
  int v1,v2,e, nedges=0;
  double totaledgelength=0, meanedgelength, meanedgelength2;

  FOR_EDGE(v1,e,v2,INNER) {
    totaledgelength += hypotD(v[v1], v[v2]);
    nedges++;
  }

  meanedgelength= totaledgelength / nedges;
  meanedgelength2= meanedgelength * meanedgelength;
//  printf("mean=%g mean^2=%g\n", meanedgelength, meanedgelength2);
    
  FOR_VERTEX(v1,OUTER) {
    FOR_VERTEX(v2,INNER) {
      if (v1 == v2) continue;

      double d2= hypotD2(v[v1],v[v2]);
      
      int dist2= sqdistances[v1][v2];
      assert(dist2>0);

      double s2= dist2 * meanedgelength2;

      /* energy = (d/s)^(1-beta)     where beta is log\_{alpha}(10)
       * energy = ((d/s)^2) ^ (1-beta)/2
       *          let beta' = (1-beta)/2
       */

      double cost= (vertex_mean_edge_lengths[v1]) * pow(d2/s2, beta_prime);
      
      //printf("layout %03x..%03x dist^2=%d s^2=%g d^2=%g "
      	       //" cost+=%g\n", v1,v2, dist2,
	       //	     s2,d2, cost);
      total_cost += cost;
    }
  }
  return total_cost/meanedgelength;
}
