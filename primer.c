/*
 * Generates the repetitive part of the gnuplot input
 * for generating the initial triangle vertices from SGT's
 * model.
 */

#include "common.h"
#include "mgraph.h"

int main(int argc, const char **argv) {
  static const int prec= DBL_DIG+2;
  int vi;
  
  if (argc!=1) { fputs("need no args\n",stderr); exit(8); }

  printf("%d %d %d %d %d\n%%-%d.%dg\n",
	 X*Y, N, X, Y, D3,
	 prec+5,prec);

  FOR_VERTEX(vi, INNER) {
    int x= vi & XMASK; /* distance along strip */
    int y= vi >> YSHIFT; /* distance across strip */
    double u= y * 1.0 / (Y-1); /* SGT's u runs 0..1 across the strip */

    /* SGT's v runs 0..pi along the strip, where the join is at 0==pi.
     * So that corresponds to 0..X (since 0==X in our scheme).
     * Vertices with odd y coordinate are halfway to the next x coordinate.
     */
    double v= (x*2 + (y&1)) * 1.0 / (X*2);
    v += 0.5;
    v *= M_PI;

    printf("%-*.*g %-*.*g # %03x %2d %2d\n",
	   prec+5,prec,u, prec+5,prec,v,
	   vi, x, y);
  }
  flushoutput();
  return 0;
}
