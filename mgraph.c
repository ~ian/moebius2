/*
 * Graph topology
 */

#include "mgraph.h"

static const unsigned dx[2][V6]= {{  +1,   0,  -1,  -1,  -1,   0  },
				  {  +1,  +1,   0,  -1,   0,  +1  }},
                      dy[V6]=     {   0, -Y1, -Y1,   0, +Y1, +Y1  };

static int edge_end2(unsigned v1, int e) {
  unsigned x, y;

  y= (v1 & ~XMASK) + dy[e];
  if (y >= Y*Y1) return -1;

  x= (v1 & XMASK) + dx[(v1 >> YSHIFT) & 1][e];
  if (x & ~XMASK) {
    //int orgy= y;
    y= (Y-1)*Y1 - y;
    x &= XMASK;;
    //printf("%40s %02x -%d-> %02x  (was %02x) \n", "", v1, e, x|y, x|orgy);
  }
  
  return x | y;
}

short edge_end2_memo[N][V6];

void mgraph_prepare(void) {
  int v, e;
  FOR_VERTEX(v, INNER)
    FOR_VPEDGE(e)
      edge_end2_memo[v][e]= edge_end2(v,e);
}

static const unsigned reverse[2][V6]= {{ 3, 4, 5, 0, 1, 2 },
				       { 3, 2, 1, 0, 5, 4 }};

int edge_reverse(int v1, int e) {
  unsigned x2;
  int flip, eprime;

  x2= (v1 & XMASK) + dx[(v1 >> YSHIFT) & 1][e];
  flip= !!(x2 & ~XMASK);
  eprime= reverse[flip][e];
//  printf("%60s %02x -%d->,<-%d- (%02x) [x2=%x flip=%d]\n","",
//	 v1,e,eprime, EDGE_END2(v1,e), x2,flip);
  return eprime;
}

int vertices_span_join_p(int v0, int v1) {
  int v0x= v0 & XMASK;
  int v1x= v1 & XMASK;
  int diff= v0x-v1x;
  return diff < -2 || diff > 2;
}
