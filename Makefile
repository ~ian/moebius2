

ALLDIMS=33 64 125 246 487 968
TARGETS= primer lumpy.cfm sgtatham.cfm ring.cfm \
	minimise-33 minimise-64 minimise-125 minimise-246 \
	$(addprefix interpolate-, $(ALLDIMS)) \
	$(addprefix view-, $(ALLDIMS)) \
	$(addprefix output-, $(ALLDIMS))
SGTATHAM=sgtatham

CWARNS=	-Wall -Wwrite-strings -Wpointer-arith -Werror -Wshadow
CXXWARNS= $(CWARNS) -Wno-shadow -Wno-error

OPTIMISE=	-O3
CFLAGS_UNIPROC=	-MMD $(OPTIMISE) -g $(CWARNS) $(DIMCFLAGS)
CXXFLAGS=	-MMD $(OPTIMISE) -g $(CXXWARNS)
CFLAGS=		$(CFLAGS_UNIPROC) $(NPROCCFLAGS)

LIBGSL= -lgsl -lgslcblas

o= >$@.new && mv -f $@.new $@

all:		$(TARGETS)

compute:	all
		$(MAKE) best-33.CFM
		$(MAKE) best-64.CFM
#		$(MAKE) best-968.CFM

minimise:	energy.o graph.o common.o mgraph.o minimise.o half.o
		$(CXX) $(CXXFLAGS) -o $@ $^ $(LIBGSL)

primer:		primer.o common.o
		$(CC) $(CFLAGS) -o $@ $^ $(LIBGSL)

nprocessors:	nprocessors.o common.o
		$(CC) $(CFLAGS_UNIPROC) -o $@ $^ $(LIBGSL)

common.o nprocessors.o: %.o: %.c
		$(CC) -c $(CPPFLAGS) $(CFLAGS_UNIPROC) $< -o $@

.nprocessors.make: ./nprocessors
	set -e; n=`./nprocessors`; \
	echo "NPROCCFLAGS := -DNPROCESSORS=$$n" $o

include .nprocessors.make


prime.data:	primer
		./$^ $o

sgtatham.cfm:	sgtatham-regenerator prime.data $(SGTATHAM)/z.typescript
		./$^ -T -o$@

lumpy.cfm: oldmoebius-converter prime.data ../moebius/ins-new ../moebius/a.out
		./$^ -o$@

ring.cfm: oldmoebius-converter prime.data /dev/null ../moebius/a.out
		./$^ -o$@

best-33.CFM:
		./minimise-33 sgtatham.cfm -iwip-33.cfm -o$@

start-64.cfm:	best-33.CFM
		./interpolate-64 -a4 $< -o$@

best-64.CFM:
		./minimise-64 start-64.cfm -iwip-64.CFM -o$@

define interpolate_aa
best-$1.cfm:	interpolate-$1 $2
		./$$< -aa $2 -o$$@
endef

$(eval $(call interpolate_aa,968, best-487.cfm))
$(eval $(call interpolate_aa,487, best-246.cfm))
$(eval $(call interpolate_aa,246, best-125.cfm))
$(eval $(call interpolate_aa,125, best-64.CFM))

view-%:		view+%.o mgraph+%.o common.o
		$(CC) $(CFLAGS) -o $@ $^ $(LIBGSL) -L/usr/X11R6/lib -lX11

output-%:	output+%.o mgraph+%.o common.o
		$(CC) $(CFLAGS) -o $@ $^ $(LIBGSL) -L/usr/X11R6/lib -lX11

interpolate-%:	interpolate+%.o mgraph+%.o common.o
		$(CC) $(CFLAGS) -o $@ $^ $(LIBGSL)

minimise-%:	energy+%.o graph+%.o mgraph+%.o minimise+%.o \
			half+%.o parallel.o common.o
		$(CXX) $(CXXFLAGS) -o $@ $^ $(LIBGSL) -lpthread

# this ridiculous repetition is due to make being too lame

view+%.o: view.c
		$(CC) -c $(CPPFLAGS) $(CFLAGS) -DDEFSZ=$* $< -o $@

output+%.o: output.c
		$(CC) -c $(CPPFLAGS) $(CFLAGS) -DDEFSZ=$* $< -o $@

mgraph+%.o: mgraph.c
		$(CC) -c $(CPPFLAGS) $(CFLAGS) -DDEFSZ=$* $< -o $@

minimise+%.o: minimise.c
		$(CC) -c $(CPPFLAGS) $(CFLAGS) -DDEFSZ=$* $< -o $@

graph+%.o: graph.c
		$(CC) -c $(CPPFLAGS) $(CFLAGS) -DDEFSZ=$* $< -o $@

energy+%.o: energy.c
		$(CC) -c $(CPPFLAGS) $(CFLAGS) -DDEFSZ=$* $< -o $@

half+%.o: half.c
		$(CC) -c $(CPPFLAGS) $(CFLAGS) -DDEFSZ=$* $< -o $@

interpolate+%.o: interpolate.c
		$(CC) -c $(CPPFLAGS) $(CFLAGS) -DDEFSZ=$* $< -o $@

.PRECIOUS: view+%.o output+%.o mgraph+%.o interpolate+%.o

clean:
		rm -f prime.data $(TARGETS)
		rm -f *.o *.new *.tmp *.rej *.orig core vgcore.* *~
		rm -f *.d .alternately_*

realclean:	clean
		rm -f *.cfm *.CFM

%.d:

-include *.d
