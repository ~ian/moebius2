/*
 * During the actual optimisation we constrain it to be symmetrical.
 * That means we have half as many vertices.  We process the vertices
 * with X >=0.
 *
 * We conventionally suffix all of our variables, macros, constants,
 * etc., with `h', to represent the double-prime ".
 *
 * Vertices with x==0 are special as they lie on the symmetry axis:
 * all but one of the coordinates are fixed.  We do not offer those
 * coordinates to our optimisation algorithm.
 *
 * We provide to the rest of the program just:
 *    NH                   number of vertices"
 *    VH2V(v)              maps a v" to the representative vertex number v
 *    FOR_VERTEX_H(vh,v)   iterates over v", computing each v
 *    DIM                  dimensionality of coordinate space
 *    [un]map_dimensions   maps double[DIM] to Vertices and back
 */

#ifndef HALF_H
#define HALF_H

#include "mgraph.h"

#define NH (N/2)
#define VH2V(v) ((v<<1) & YMASK | v & (XMASK>>1))
#define FOR_VERTEX_H(vh,v) \
  for ((vh)=0; (v)= VH2V((vh)), (vh) < NH; (vh)++)

#define HALF_DEGRADED_NVERTICES ((Y+1)/2)
#define DIM (NH*D3 + HALF_DEGRADED_NVERTICES*(1-D3) + ((Y+1)/2)*D3)

void unmap_dimensions(double metavertex[DIM], const struct Vertices *in);
void map_dimensions(const double metavertex[DIM], Vertices out);
void pmap_dimensions(const struct Vertices *vs);

#endif /*HALF_H*/
