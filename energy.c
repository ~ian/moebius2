/*
 * We try to find an optimal triangle grid
 */

#include "common.h"
#include "minimise.h"
#include "mgraph.h"
#include "parallel.h"

double vertex_mean_edge_lengths[N];

static double vertex_areas[N];
static double edge_lengths[N][V6];
static double rim_vertex_angles[N];

static double best_energy= DBL_MAX;

static void addcost(double *energy, double tweight, double tcost, int pr);

/*---------- main energy computation, weights, etc. ----------*/

typedef double CostComputation(const Vertices vertices, int section);
typedef void PreComputation(const Vertices vertices, int section);

typedef struct {
  double weight;
  CostComputation *fn;
} CostContribution;

#define NPRECOMPS ((sizeof(precomps)/sizeof(precomps[0])))
#define NCOSTS ((sizeof(costs)/sizeof(costs[0])))
#define COST(weight, compute) { (weight),(compute) },

static PreComputation *const precomps[]= {
  compute_edge_lengths,
  compute_vertex_areas,
  compute_rim_twist_angles
};

static const CostContribution costs[]= {

#if XBITS==3
#define STOP_EPSILON 1e-6
    COST(  3e3,   vertex_displacement_cost)
    COST( 0.4e3,  rim_proximity_cost)
    COST(  1e7,   edge_angle_cost)
                  #define EDGE_ANGLE_COST_CIRCCIRCRAT (0.2/1.7)
    COST(  1e2,   small_triangles_cost)
    COST(  1e12,   noncircular_rim_cost)
#endif

#if XBITS==4
#define STOP_EPSILON 5e-3
    COST(  3e4,   vertex_displacement_cost) // NB this is probably wrong now
    COST(  3e4,   vertex_edgewise_displ_cost) // we have changed the power
    COST( 0.2e3,  rim_proximity_cost)
    COST( 1e4,  rim_twist_cost)
    COST(  1e12,   noncircular_rim_cost)
    COST(  10e1,   nonequilateral_triangles_cost)
//    COST(  1e1,   small_triangles_cost)
//    COST(  1e6,   edge_angle_cost)
                  #define EDGE_ANGLE_COST_CIRCCIRCRAT (0.5/1.7)
#endif

#if XBITS==5
#define STOP_EPSILON 1e-5
    COST(  3e4,   vertex_displacement_cost)
    COST(  3e4,   vertex_edgewise_displ_cost)
    COST(  2e-1,  rim_proximity_cost)
    COST(  3e3,  rim_twist_cost)
    COST(  1e12,   noncircular_rim_cost)
    COST(   3e2,   nonequilateral_triangles_cost)
//    COST(  1e1,   small_triangles_cost)
//    COST(  1e6,   edge_angle_cost)
                  #define EDGE_ANGLE_COST_CIRCCIRCRAT (0.5/1.7)
#endif

#if XBITS==6
#define STOP_EPSILON 1e-4
    COST(  3e5,   vertex_displacement_cost)
    COST(  3e5,   vertex_edgewise_displ_cost)
    COST( 3e-2,  rim_proximity_cost)
    COST(  1e4,  rim_twist_cost)
    COST(  1e12,   noncircular_rim_cost)
    COST(  10e1,   nonequilateral_triangles_cost)
//    COST(  1e1,   small_triangles_cost)
//    COST(  1e6,   edge_angle_cost)
                  #define EDGE_ANGLE_COST_CIRCCIRCRAT (0.5/1.7)
#endif

#if XBITS>=7 /* nonsense follows but never mind */
#define STOP_EPSILON 1e-6
    COST(  3e5,   line_bending_cost)
    COST( 10e2,   edge_length_variation_cost)
    COST( 9.0e1,  rim_proximity_cost) // 5e1 is too much
				      // 2.5e1 is too little
    // 0.2e1 grows compared to previous ?
    // 0.6e0 shrinks compared to previous ?

    COST(  1e12,   edge_angle_cost)
                  #define EDGE_ANGLE_COST_CIRCCIRCRAT (0.5/1.3)
    COST(  1e18,   noncircular_rim_cost)
#endif

};

const double edge_angle_cost_circcircrat= EDGE_ANGLE_COST_CIRCCIRCRAT;

void energy_init(void) {
  stop_epsilon= STOP_EPSILON;
}

/*---------- energy computation machinery ----------*/

void compute_energy_separately(const struct Vertices *vs,
			 int section, void *energies_v, void *totals_v) {
  double *energies= energies_v;
  int ci;
  
  for (ci=0; ci<NPRECOMPS; ci++) {
    precomps[ci](vs->a, section);
    inparallel_barrier();
  }
  for (ci=0; ci<NCOSTS; ci++)
    energies[ci]= costs[ci].fn(vs->a, section);
}

void compute_energy_combine(const struct Vertices *vertices,
			 int section, void *energies_v, void *totals_v) {
  int ci;
  double *energies= energies_v;
  double *totals= totals_v;

  for (ci=0; ci<NCOSTS; ci++)
    totals[ci] += energies[ci];
}

double compute_energy(const struct Vertices *vs) {
  static int bests_unprinted;

  double totals[NCOSTS], energy;
  int ci, printing;

  printing= printing_check(pr_cost,0);

  if (printing) printf("%15lld c>e |", evaluations);

  for (ci=0; ci<NCOSTS; ci++)
    totals[ci]= 0;

  inparallel(vs,
	     compute_energy_separately,
	     compute_energy_combine,
	     sizeof(totals) /* really, size of energies */,
	     totals);

  energy= 0;
  for (ci=0; ci<NCOSTS; ci++)
    addcost(&energy, costs[ci].weight, totals[ci], printing);

  if (printing) printf("| total %# e |", energy);

  if (energy < best_energy) {
    FILE *best_f;
    int r;

    if (printing) {
      printf(" BEST");
      if (bests_unprinted) printf(" [%4d]",bests_unprinted);
      bests_unprinted= 0;
    } else {
      bests_unprinted++;
    }

    best_f= fopen(best_file_tmp,"wb");  if (!best_f) diee("fopen new out");
    r= fwrite(vs->a,sizeof(vs->a),1,best_f); if (r!=1) diee("fwrite");
    if (fclose(best_f)) diee("fclose new best");
    if (rename(best_file_tmp,best_file)) diee("rename install new best");

    best_energy= energy;
  }
  if (printing) {
    putchar('\n');
    flushoutput();
  }

  evaluations++;
  return energy;
}

static void addcost(double *energy, double tweight, double tcost, int pr) {
  double tenergy= tweight * tcost;
  if (pr) printf(/*" %# e >"*/ " %# e* |", /*tcost,*/ tenergy);
  *energy += tenergy;
}

/*---------- Precomputations ----------*/

void compute_edge_lengths(const Vertices vertices, int section) {
  int v1,e,v2;

  FOR_EDGE(v1,e,v2, OUTER)
    edge_lengths[v1][e]= hypotD(vertices[v1],vertices[v2]);
}

void compute_vertex_areas(const Vertices vertices, int section) {
  int v0,v1,v2, e1,e2;
//  int k;

  FOR_VERTEX(v0, OUTER) {
    double total= 0.0, edges_total=0;
    int count= 0;

    FOR_VEDGE(v0,e1,v1) {
      e2= (e1+1) % V6;
      v2= EDGE_END2(v0,e2);
      if (v2<0) continue;

      edges_total += edge_lengths[v0][e1];

//      double e1v[D3], e2v[D3], av[D3];
//      K {
//	e1v[k]= vertices[v1][k] - vertices[v0][k];
//	e2v[k]= vertices[v2][k] - vertices[v0][k];
//      }
//      xprod(av, e1v, e2v);
//      total += magnD(av);

      count++;
    }
    vertex_areas[v0]= total / count;
    vertex_mean_edge_lengths[v0]= edges_total / count;
  }
}

/*---------- displacement of vertices across a midpoint ----------*/

  /*
   * Subroutine used where we have
   *
   *        R - - - - - - - M . -  -  -  -  R'
   *                            ` .
   *                                ` .
   *                                    S
   *
   * and wish to say that the vector RM should be similar to MS
   * or to put it another way S = M + RM
   *
   * NB this is not symmetric wrt R and S since it divides by
   * |SM| but not |RM| so you probably want to call it twice.
   *
   * Details:
   *
   *   Let  R' = M + SM
   *        D  = R' - R
   *
   * Then the (1/delta)th power of the cost is
   *      proportional to |D|, and
   *      inversely proportional to |SM|
   * except that |D| is measured in a wierd way which counts
   * distance in the same direction as SM 1/lambda times as much
   * ie the equipotential surfaces are ellipsoids around R',
   * lengthened by lambda in the direction of RM.
   *
   * So
   *                                                               delta
   *                [       -1                                    ]
   *   cost      =  [ lambda  . ( D . SM/|SM| ) + | D x SM/|SM| | ]
   *       R,S,M    [ ------------------------------------------- ]
   *                [                      |SM|                   ]
   *
   */

static double vertex_one_displ_cost(const double r[D3], const double s[D3],
				    const double midpoint[D3],
				    double delta, double inv_lambda) {
  const double smlen2_epsilon= 1e-12;
  double sm[D3], d[D3], ddot, dcross[D3];
  int k;

  K sm[k]= -s[k] + midpoint[k];
  K d[k]= midpoint[k] + sm[k] - r[k];
  ddot= dotprod(d,sm);
  xprod(dcross, d,sm);
  double smlen2= magnD2(sm);
  double cost_basis= inv_lambda * ddot + magnD(dcross);
  double cost= pow(cost_basis / (smlen2 + smlen2_epsilon), delta);

  return cost;
}

/*---------- displacement of vertices opposite at a vertex ----------*/

  /*
   *   At vertex Q considering edge direction e to R
   *   and corresponding opposite edge to S.
   *
   *   This is vertex displacement as above with M=Q
   */

double vertex_displacement_cost(const Vertices vertices, int section) {
  const double inv_lambda= 1.0/1; //2;
  const double delta= 6;

  int si,e,qi,ri;
  double total_cost= 0;

  FOR_EDGE(qi,e,ri, OUTER) {
    si= EDGE_END2(qi,(e+3)%V6); if (si<0) continue;

    total_cost += vertex_one_displ_cost(vertices[ri], vertices[si], vertices[qi],
					delta, inv_lambda);
  }
  return total_cost;
}

/*---------- displacement of vertices opposite at an edge ----------*/

  /*
   *   At edge PQ considering vertices R and S (see diagram
   *   below for overly sharp edge cost).
   *
   *   Let  M  = midpoint of PQ
   */

double vertex_edgewise_displ_cost(const Vertices vertices, int section) {
  const double inv_lambda= 1.0/1; //2;
  const double delta= 6;

  int pi,e,qi,ri,si, k;
  double m[D3];
  double total_cost= 0;

  FOR_EDGE(pi,e,qi, OUTER) {
    si= EDGE_END2(pi,(e+V6-1)%V6);  if (si<0) continue;
    ri= EDGE_END2(pi,(e   +1)%V6);  if (ri<0) continue;

    K m[k]= 0.5 * (vertices[pi][k] + vertices[qi][k]);
    
    total_cost += vertex_one_displ_cost(vertices[ri], vertices[si], m,
					delta, inv_lambda);
  }
  return total_cost;
}


/*---------- at-vertex edge angles ----------*/

  /*
   * Definition:
   *
   *    At each vertex Q, in each direction e:
   *
   *                   	              e
   *                           Q ----->----- R
   *		          _,-'\__/
   *   	       	      _,-'     	 delta
   * 		   P '
   *
   *                      r
   *       cost    = delta          (we use r=3)
   *           Q,e
   *
   *
   * Calculation:
   *
   *      Let vector A = PQ
   *                 B = QR
   *
   *   		       -1   A . B
   *      delta =  tan     -------
   *     		  | A x B |
   *
   *      which is always in the range 0..pi because the denominator
   *      is nonnegative.  We add epsilon to |AxB| to avoid division
   *      by zero.
   *
   *                     r
   *      cost    = delta
   *          Q,e
   */

double line_bending_cost(const Vertices vertices, int section) {
  static const double axb_epsilon= 1e-6;
  static const double exponent_r= 4;

  int pi,e,qi,ri, k;
  double  a[D3], b[D3], axb[D3];
  double total_cost= 0;

  FOR_EDGE(qi,e,ri, OUTER) {
    pi= EDGE_END2(qi,(e+3)%V6); if (pi<0) continue;

//if (!(qi&XMASK)) fprintf(stderr,"%02x-%02x-%02x (%d)\n",pi,qi,ri,e);

    K a[k]= -vertices[pi][k] + vertices[qi][k];
    K b[k]= -vertices[qi][k] + vertices[ri][k];

    xprod(axb,a,b);

    double delta= atan2(magnD(axb) + axb_epsilon, dotprod(a,b));
    double cost= pow(delta,exponent_r);

    total_cost += cost;
  }
  return total_cost;
}

/*---------- edge length variation ----------*/

  /*
   * Definition:
   *
   *    See the diagram above.
   *                                r
   *       cost    = ( |PQ| - |QR| )
   *           Q,e
   */

double edge_length_variation_cost(const Vertices vertices, int section) {
  double diff, cost= 0, exponent_r= 2;
  int q, e,r, eback;

  FOR_EDGE(q,e,r, OUTER) {
    eback= edge_reverse(q,e);
    diff= edge_lengths[q][e] - edge_lengths[q][eback];
    cost += pow(diff,exponent_r);
  }
  return cost;
}

/*---------- proportional edge length variation ----------*/

  /*
   * Definition:
   *
   *    See the diagram above.
   *                                r
   *       cost    = ( |PQ| - |QR| )
   *           Q,e
   */

double prop_edge_length_variation_cost(const Vertices vertices, int section) {
  const double num_epsilon= 1e-6;

  double cost= 0, exponent_r= 2;
  int q, e,r, eback;

  FOR_EDGE(q,e,r, OUTER) {
    eback= edge_reverse(q,e);
    double le= edge_lengths[q][e];
    double leback= edge_lengths[q][eback];
    double diff= le - leback;
    double num= MIN(le, leback);
    cost += pow(diff / (num + num_epsilon), exponent_r);
  }
  return cost;
}

/*---------- rim proximity cost ----------*/

static void find_nearest_oncircle(double oncircle[D3], const double p[D3]) {
  /* By symmetry, nearest point on circle is the one with
   * the same angle subtended at the z axis. */
  oncircle[0]= p[0];
  oncircle[1]= p[1];
  oncircle[2]= 0;
  double mult= 1.0/ magnD(oncircle);
  oncircle[0] *= mult;
  oncircle[1] *= mult;
}

double rim_proximity_cost(const Vertices vertices, int section) {
  double oncircle[D3], cost=0;
  int v;

  FOR_VERTEX(v, OUTER) {
    int y= v >> YSHIFT;
    int nominal_edge_distance= y <= Y/2 ? y : Y-1-y;
    if (nominal_edge_distance==0) continue;

    find_nearest_oncircle(oncircle, vertices[v]);

    cost +=
      vertex_mean_edge_lengths[v] *
      (nominal_edge_distance*nominal_edge_distance) /
      (hypotD2(vertices[v], oncircle) + 1e-6);
  }
  return cost;
}

/*---------- noncircular rim cost ----------*/

double noncircular_rim_cost(const Vertices vertices, int section) {
  int vy,vx,v;
  double cost= 0.0;
  double oncircle[3];

  FOR_RIM_VERTEX(vy,vx,v, OUTER) {
    find_nearest_oncircle(oncircle, vertices[v]);

    double d2= hypotD2(vertices[v], oncircle);
    cost += d2*d2;
  }
  return cost;
}

/*---------- rim contact angle rotation ----------*/

void compute_rim_twist_angles(const Vertices vertices, int section) {
  double oncircle[D3], distance[D3];
  int vpy,vpx,v,k;
    
  FOR_NEAR_RIM_VERTEX(vpy,vpx,v, 1,OUTER) {
    find_nearest_oncircle(oncircle, vertices[v]);
    /* we are interested in the angle subtended at the rim, from the
     * rim's point of view. */
    K distance[k]= vertices[v][k] - oncircle[k];
    double distance_positive_z= distance[3];
    double distance_radial_outwards= dotprod(distance, oncircle);
    rim_vertex_angles[v]= atan2(distance_positive_z, distance_radial_outwards);
  }
}

double rim_twist_cost(const Vertices vertices, int section) {
  double total_cost= 0;
  int vpy,vpx,v0,v1;
  
  FOR_NEAR_RIM_VERTEX(vpy,vpx,v0, 1,OUTER) {
    v1= EDGE_END2(v0,0);  assert(v1!=0);
    double delta= rim_vertex_angles[v0] - rim_vertex_angles[v1];
    if (delta < M_PI) delta += 2*M_PI;
    if (delta > M_PI) delta -= 2*M_PI;

    double cost= pow(delta, 4);
    total_cost += cost;
  }

  return total_cost;
}

/*---------- overly sharp edge cost ----------*/

  /*
   *
   *   	            Q `-_
   *              / |    `-_		               P'Q' ------ S'
   *             /  |       `-.		             _,' `.       .
   *            /   |           S	          _,'     :      .
   *           /    |      _,-'                _,'        :r    .r
   *          /     |  _,-'		   R' '           `.   .
   *         /    , P '			       ` .   r     :  .
   *        /  ,-'			            `  .   : 
   *       /,-'				                 ` C'
   *      /'
   *   	 R
   *
   *
   *
   *  Let delta =  angle between two triangles' normals
   *
   *  Giving energy contribution:
   *
   *                                   2
   *    E             =  F   .  delta
   *     vd, edge PQ      vd
   */

double edge_angle_cost(const Vertices vertices, int section) {
  double pq1[D3], rp[D3], ps[D3], rp_2d[D3], ps_2d[D3], rs_2d[D3];
  double a,b,c,s,r;
  const double minradius_base= 0.2;

  int pi,e,qi,ri,si, k;
//  double our_epsilon=1e-6;
  double total_cost= 0;

  FOR_EDGE(pi,e,qi, OUTER) {
//    if (!(RIM_VERTEX_P(pi) || RIM_VERTEX_P(qi))) continue;

    si= EDGE_END2(pi,(e+V6-1)%V6);  if (si<0) continue;
    ri= EDGE_END2(pi,(e   +1)%V6);  if (ri<0) continue;

    K {
      pq1[k]= -vertices[pi][k] + vertices[qi][k];
      rp[k]=  -vertices[ri][k] + vertices[pi][k];
      ps[k]=  -vertices[pi][k] + vertices[si][k];
    }

    normalise(pq1,1,1e-6);
    xprod(rp_2d, rp,pq1); /* projects RP into plane normal to PQ */
    xprod(ps_2d, ps,pq1); /* likewise PS */
    K rs_2d[k]= rp_2d[k] + ps_2d[k];
    /* radius of circumcircle of R'P'S' from Wikipedia
     * `Circumscribed circle' */
    a= magnD(rp_2d);
    b= magnD(ps_2d);
    c= magnD(rs_2d);
    s= 0.5*(a+b+c);
    r= a*b*c / sqrt((a+b+c)*(a-b+c)*(b-c+a)*(c-a+b) + 1e-6);

    double minradius= minradius_base + edge_angle_cost_circcircrat*(a+b);
    double deficit= minradius - r;
    if (deficit < 0) continue;
    double cost= deficit*deficit;

    total_cost += cost;
  }

  return total_cost;
}

/*---------- small triangles cost ----------*/

  /*
   * Consider a triangle PQS
   *
   * Cost is 1/( area^2 )
   */

double small_triangles_cost(const Vertices vertices, int section) {
  double pq[D3], ps[D3];
  double x[D3];
  int pi,e,qi,si, k;
//  double our_epsilon=1e-6;
  double total_cost= 0;

  FOR_EDGE(pi,e,qi, OUTER) {
//    if (!(RIM_VERTEX_P(pi) || RIM_VERTEX_P(qi))) continue;

    si= EDGE_END2(pi,(e+V6-1)%V6);  if (si<0) continue;

    K {
      pq[k]= vertices[qi][k] - vertices[pi][k];
      ps[k]= vertices[si][k] - vertices[pi][k];
    }
    xprod(x, pq,ps);

    double cost= 1/(magnD2(x) + 0.01);

//double cost= pow(magnD(spqxpqr), 3);
//assert(dot>=-1 && dot <=1);
//double cost= 1-dot;
    total_cost += cost;
  }

  return total_cost;
}

/*---------- nonequilateral triangles cost ----------*/

  /*
   * Consider a triangle PQR
   *
   * let edge lengths a=|PQ| b=|QR| c=|RP|
   *
   * predicted edge length p = 1/3 * (a+b+c)
   *
   * compute cost for each x in {a,b,c}
   *
   *
   *      cost      = (x-p)^2 / p^2
   *          PQR,x
   */

double nonequilateral_triangles_cost(const Vertices vertices, int section) {
  double pr[D3], abc[3];
  int pi,e0,e1,qi,ri, k,i;
  double our_epsilon=1e-6;
  double total_cost= 0;

  FOR_EDGE(pi,e0,qi, OUTER) {
    e1= (e0+V6-1)%V6;
    ri= EDGE_END2(pi,e1);  if (ri<0) continue;

    K pr[k]= -vertices[pi][k] + vertices[ri][k];

    abc[0]= edge_lengths[pi][e0]; /* PQ */
    abc[1]= edge_lengths[qi][e1]; /* QR */
    abc[2]= magnD(pr);

    double p= (1/3.0) * (abc[0]+abc[1]+abc[2]);
    double p_inv2= 1/(p*p + our_epsilon);

    for (i=0; i<3; i++) {
      double diff= (abc[i] - p);
      double cost= diff*diff * p_inv2;
      total_cost += cost;
    }
  }

  return total_cost;
}
