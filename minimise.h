/*
 * Routines used for minimisation (only)
 */

#ifndef MINIMISE_H
#define MINIMISE_H

#include "mgraph.h"

double compute_energy(const struct Vertices *vs);
void energy_init(void);

double graph_layout_cost(const Vertices v, int section);
void graph_layout_prepare();

void compute_vertex_areas(const Vertices vertices, int section);
void compute_edge_lengths(const Vertices vertices, int section);
void compute_rim_twist_angles(const Vertices vertices, int section);

//extern double vertex_areas[N], edge_lengths[N][V6];
extern double vertex_mean_edge_lengths[N];

extern const double edge_angle_cost_circcircrat;

double vertex_displacement_cost(const Vertices vertices, int section);
double vertex_edgewise_displ_cost(const Vertices vertices, int section);
double line_bending_cost(const Vertices vertices, int section);
double noncircular_rim_cost(const Vertices vertices, int section);
double edge_length_variation_cost(const Vertices vertices, int section);
double prop_edge_length_variation_cost(const Vertices vertices, int section);
double rim_proximity_cost(const Vertices vertices, int section);
double rim_twist_cost(const Vertices vertices, int section);
double edge_angle_cost(const Vertices vertices, int section);
double small_triangles_cost(const Vertices vertices, int section);
double nonequilateral_triangles_cost(const Vertices vertices, int section);

extern const char *input_file, *best_file;
extern char *best_file_tmp;
extern long long evaluations;
extern double stop_epsilon;

enum printing_instance { pr_cost, pr_size, pr__max };
int printing_check(enum printing_instance, int indent);

#endif /*MINIMISE_H*/
